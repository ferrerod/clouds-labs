import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

local    = pd.read_csv("locust_out/local_stats_history.csv")
scaleset = pd.read_csv("locust_out/scaleset_stats_history.csv")
webapp   = pd.read_csv("locust_out/webapp_stats_history.csv")

plt.plot(np.arange(1, 1+local.shape[0]), local['Requests/s'], label='local')
plt.plot(np.arange(1, 1+scaleset.shape[0]), scaleset['Requests/s'], label='scaleset')
plt.plot(np.arange(1, 1+webapp.shape[0]), webapp['Requests/s'], label='webapp')

plt.xlabel('seconds')
plt.ylabel('Successful Requests')

plt.legend()

plt.show()
