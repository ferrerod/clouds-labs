import logging

import azure.functions as func
from math import sin

def integrate(lower, upper):
    f = lambda x : abs(sin(x))
    N = [10*z for z in range(1,8)]
    res = list()
    upper = float(upper)
    lower = float(lower)
    for n in N:
        dx = (upper - lower) / n
        integral = 0.0
        for i in range(n):
            xip12 = dx * (i + 0.5)
            dI = f(xip12) * dx
            integral += dI
        res.append(integral)
    return res

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    name = req.params.get('name')
    lower = req.params.get('lower')
    upper = req.params.get('upper')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')

    if name:
        return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
    elif lower and upper:
        return func.HttpResponse(f"{integrate(lower, upper)}", status_code=200)
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
             status_code=200
        )
