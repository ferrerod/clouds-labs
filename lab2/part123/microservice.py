from flask import Flask
from integration import integrate

app = Flask(__name__)

@app.route('/numericalintegralservice/<lower>/<upper>')
def service(lower, upper):
    results = integrate(float(lower), float(upper))
    return str(results)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)