from math import sin

def integrate(lower, upper):
    f = lambda x : abs(sin(x))
    N = [10*z for z in range(1,8)]
    res = list()
    for n in N:
        dx = (upper - lower) / n
        integral = 0.0
        for i in range(n):
            xip12 = dx * (i + 0.5)
            dI = f(xip12) * dx
            integral += dI
        res.append(integral)
    return res